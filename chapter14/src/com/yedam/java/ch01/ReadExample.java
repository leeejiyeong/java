package com.yedam.java.ch01;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.Reader;

public class ReadExample {		//출력하기
	
	public static void main(String[] args) throws Exception{
		InputStream is = new FileInputStream("C:/Temp/test1.db");
		
//		//그냥 한개만 출력하기
//		int data = is.read();
//		System.out.println(data);
		
		//여러개 적은거 한글자 한글자 다 출력하려면 while문으로 돌리면된다.
		while(true) {
			
			int data = is.read();
			if(data == -1) break;
			System.out.println(data);
			
		}
		System.out.println();
		is.close();
		
		//다시 읽어들이고 싶으면 ↑스트림을 닫았다가 ↓다시 열어야한다
		is = new FileInputStream("C:/Temp/test1.db");
		
		//한꺼번에 읽어들이기
		byte[] buffer = new byte[100];
		while(true) {
			int readByteNum = is.read(buffer);		//몇글자 읽었는지를 알려줌
			if(readByteNum == -1) break;
			for(int i=0; i< readByteNum; i++) {
				System.out.println(buffer[i]);
			}
		}
		
		is.close();
		
		Reader reader = new FileReader("C:/Temp/test2.txt");
		while(true) {
			int data = reader.read();
			if(data == -1) break;
			System.out.print((char)data);
		}
		reader.close();
		
		System.out.println();
		
		//
		reader = new FileReader("C:/Temp/test2.txt");
		
		char[] charBuffer = new char[100];
		while(true) {
			int readCharNum = reader.read(charBuffer);
			if(readCharNum == -1) break;
			for(int i=0; i < readCharNum; i++) {
				System.out.print(charBuffer[i]);
			}
		}
		reader.close();
	}
}
