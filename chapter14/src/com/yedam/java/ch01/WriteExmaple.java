package com.yedam.java.ch01;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.Writer;

public class WriteExmaple {		//입력하기
	
	public static void main(String[] args) throws Exception {
		OutputStream os = new FileOutputStream("C:/Temp/test1.db");		//모니터가아니라 파일에 무언가를 입력하려고 한다
		
		byte a = 10;
		byte b = 20;
		byte c = 30;
		
		//한개씩 보내기
		os.write(a);
		os.write(b);
		os.write(c);
		
		//한꺼번에 보내기(배열사용)
		byte[] byteAray = {40,50,60};
		//배열중에 인덱스1 부터 2개만 보내기
		os.write(byteAray, 1, 2);
		
		os.flush();
		os.close();		//항상 flush하고 close하기
		
		//-----------------------
		
		Writer writer = new FileWriter("C:/Temp/test2.txt");
		
		char ca = 'A';
		char cb = 'B';
		
		writer.write(ca);
		writer.write(cb);
		
		char[] charAray = {'C', 'D', 'E'};
		writer.write(charAray, 1, 2);
		
		String str = "\nHello World!";
		writer.write(str);
		
		writer.flush();
		writer.close();
		
		
	}
}
