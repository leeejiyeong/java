package com.yedam.java.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SelectExample {
	/***
	 * sqlite랑 다른거는  1. driver loading이랑 2. db접속하기 뿐이다.
	 * ***/

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		//1.JDBC Driver 로딩하기
		Class.forName("oracle.jdbc.driver.OracleDriver");
		
		//2. DB 서버 접속하기
		String url = "jdbc:oracle:thin:@localhost:1521:xe";
		String id = "hr";
		String password = "hr";		//오라클은 보안상 계정입력을 해야한다.
		
		Connection conn = DriverManager.getConnection(url, id, password);
		
		//3. Statement or PreparedStatement 사용해서 객체 생성하기
		Statement stmt = conn.createStatement();		//sql문 없이 statement먼저 실행되는게 특이점
		
		//4. SQL 실행
		String sql = "SELECT * FROM employees";
		ResultSet rs = stmt.executeQuery(sql);
		
		//5. 결과값 출력하기
		while(rs.next()) {		//next를 이용해서 다음이 있다면 값을 꺼내오는것
			String name = "이름 : " + rs.getString("first_name") + " " + rs.getString("last_name");
			System.out.println(name);
		}
		
		//6. 자원 해제하기	 -  추가한 역순으로 닫아줌
		if(rs != null) rs.close();
		if(stmt != null) stmt.close();
		if(conn != null) conn.close();
		
	}

}
