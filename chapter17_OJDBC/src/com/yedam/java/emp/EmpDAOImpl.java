package com.yedam.java.emp;

import java.util.ArrayList;
import java.util.List;

import com.yedam.java.common.DAO;

public class EmpDAOImpl extends DAO implements EmpDAO{
	//싱글톤
		private static EmpDAO instance = null;
		
		public static EmpDAO getInstance() {
			if(instance == null)
				instance = new EmpDAOImpl();
			return instance;
		}
		
	@Override
	public List<EmpVO> selectAll() {
		List<EmpVO> list = new ArrayList<>();	//골격먼저 만들고 내용을 채우자.
		try {
			connect();
			
			stmt = conn.createStatement();
			String sql = "SELECT * FROM employees";
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				EmpVO empVO = new EmpVO();
				empVO.setEmployeeId(rs.getInt("employee_id"));
				empVO.setFirstName(rs.getString("first_name"));
				empVO.setLastName(rs.getString("last_name"));
				empVO.setEmail(rs.getString("email"));
				empVO.setPhoneNumber(rs.getString("phone_number"));
				empVO.setHireDate(rs.getDate("hire_date"));
				empVO.setJobId(rs.getString("job_id"));
				empVO.setSalary(rs.getDouble("salary"));
				empVO.setCommissionPct(rs.getDouble("commission_pct"));
				empVO.setManagerId(rs.getInt("manager_id"));
				empVO.setDepartmentId(rs.getInt("department_id"));	
				
				list.add(empVO);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			disconnect();
		}
		return list;		//리턴 부터 써주자
	}

	@Override
	public EmpVO selectEmpInfo(EmpVO empVO) {
		EmpVO findEmp = null;
		try {
			connect();
			//stmt - 비어있는 공간에 만든다. 물음표 안씀. 문자, 날짜 타입 넘겨줄때 주의해야한다
			stmt = conn.createStatement();
			String sql =  "SELECT * FROM employees WHERE employee_id = " + empVO.getEmployeeId();
				//"SELECT * FROM employees WHERE job_id = '" + empVO.getJobId() + "'"; 문자타입은 그냥쓰면 오류난다. 홀따옴표 써줘야함
			rs = stmt.executeQuery(sql);	// ()안에sql넣음
			
			//pstmt - 문자타입의 경우 물음표안에 따옴표를 포함해서 넣어주기 때문에 문자 타입 신경안써도됨
//			String sql2 =  "SELECT * FROM employees WHERE employee_id = ?";
//			pstmt = conn.prepareStatement(sql2);
//			pstmt.setInt(1, empVO.getEmployeeId());
//			rs = pstmt.executeQuery();		// ()안에sql안넣음
			
			if(rs.next()) {		//하나만 요구할때 if사용
				findEmp = new EmpVO();
				findEmp.setEmployeeId(rs.getInt("employee_id"));
				findEmp.setFirstName(rs.getString("first_name"));
				findEmp.setLastName(rs.getString("last_name"));
				findEmp.setEmail(rs.getString("email"));
				findEmp.setPhoneNumber(rs.getString("phone_number"));
				findEmp.setHireDate(rs.getDate("hire_date"));
				findEmp.setJobId(rs.getString("job_id"));
				findEmp.setSalary(rs.getDouble("salary"));
				findEmp.setCommissionPct(rs.getDouble("commission_pct"));
				findEmp.setManagerId(rs.getInt("manager_id"));
				findEmp.setDepartmentId(rs.getInt("department_id"));				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			disconnect();
		}
		return findEmp;
	}

	@Override
	public void insertEmpInfo(EmpVO empVO) {
		try {
			connect();
			String sql = "INSERT INTO employees VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, empVO.getEmployeeId());	
			pstmt.setString(2, empVO.getFirstName());
			pstmt.setString(3, empVO.getLastName());
			pstmt.setString(4, empVO.getEmail());
			pstmt.setString(5, empVO.getPhoneNumber());
			pstmt.setDate(6, empVO.getHireDate());
			pstmt.setString(7, empVO.getJobId());
			pstmt.setDouble(8, empVO.getSalary());
			pstmt.setDouble(9, empVO.getCommissionPct());
			pstmt.setInt(10, empVO.getManagerId());
			pstmt.setInt(11, empVO.getDepartmentId());
			
			int result = pstmt.executeUpdate();
			 
			if(result > 0) {
				System.out.println("정상적으로 등록되었습니다.");
			}else {
				System.out.println("정상적으로 등록되지 않았습니다.");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		
	}

	@Override
	public void updateEmpInfo(EmpVO empVO) {
		try {
			connect();
			String sql = "UPDATE employees SET first_name = ? WHERE employee_id = ? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, empVO.getFirstName());
			pstmt.setInt(2, empVO.getEmployeeId());
			
			int result = pstmt.executeUpdate();
			
			if(result > 0) {
				System.out.println("정상적으로 수정되었습니다.");
			}else {
				System.out.println("정상적으로 수정되지 않았습니다.");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		
	}

	@Override
	public void deleteEmpInfo(EmpVO empVO) {
		try {
			connect();
			stmt = conn.createStatement();
			String sql = "DELETE FROM employees WHERE employee_id = " + empVO.getEmployeeId();
			int result = stmt.executeUpdate(sql);
			
			if(result > 0) {
				System.out.println("정상적으로 삭제되었습니다.");
			}else {
				System.out.println("정상적으로 삭제되지 않았습니다.");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconnect();
		}
		
	}


}
